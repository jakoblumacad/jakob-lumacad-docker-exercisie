FROM azul/zulu-openjdk-alpine:11

VOLUME /tmp

ADD "./target/docker-exercise-0.0.1-SNAPSHOT.jar" "./app.jar"

EXPOSE 8085

CMD ["java", "-jar", "-Djava.security.egd=file:/dev/./urandom", "./app.jar"]